#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

#define BUFFER_SIZE 1000

int argcount(char *str)
{
    int count = 0;

    if (*str == '\n') return 0;

    for (char *cp = str; *cp != '\n'; cp++)
        if ((*(cp+1) == ' ' || *(cp+1) == '\n') && *cp != ' ')
            count++;

	return count;
}

void parse_argv(char **argv, char *cmdline) 
{
    char *word_start;
    char *cp;
    int c = 0;

    if (*cmdline != ' ') word_start = cmdline;

    for (cp = cmdline+1; *cp != '\n'; cp++) {
        if (*(cp-1) == ' ' && *cp != ' ')
            word_start = cp;

        if (*(cp-1) != ' ' && *cp == ' ') {
            *(argv+c) = (char *) malloc((1 + cp - word_start) * sizeof (char));
            memcpy (*(argv+c), word_start, (cp - word_start));
            *(*(argv+c)+(cp - word_start)) = '\0';
            c++;
        }
    }
    *(argv+c) = (char *) malloc((1 + cp - word_start) * sizeof (char));
    memcpy (*(argv+c), word_start, (cp - word_start));
    *(*(argv+c)+(cp - word_start)) = '\0';

    *(argv+c+1) = (char *) malloc(sizeof (char));
    *(argv+c+1) = NULL;
}

int main(void)
{ 
    char cmdline[BUFFER_SIZE] = {};
    pid_t pid;
    char *r, *delim;

    while (1) {
        printf("\033[01m$\033[m ");
            r = fgets(cmdline, BUFFER_SIZE, stdin);

            if (!r) {
                // Exit shell
                printf("\n");
                break;
            }

	    int argc = 0;

        char **argv;
        char *word_start, *word_end;

        argc = argcount(cmdline);
        if (!argc)
            continue;

        argv = (char **) malloc(argc * sizeof (char *));
        parse_argv(argv, cmdline);

        pid = fork();
        if (!pid)
        {
            // Child
            execvp(argv[0], argv);
            exit(1);
        }

        for (int i = 0; i < argc; i++)
            free(argv[i]);

        free(argv);
        waitpid(pid, NULL, 0);
    }

    return 0;
}
